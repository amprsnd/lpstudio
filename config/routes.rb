Rails.application.routes.draw do

  scope '(:locale)' do
    root 'output#index'
    get '/p/:id', to: 'output#show'
    get '/(:locale/)dashboard', to: 'dashboard#index'
  end

  devise_for :admins,
             :path => '(:locale)/dashboard',
             :path_names => {
                 :sign_in => 'login',
                 :sign_out => 'logout',
                 :sign_up => 'register'
             }

  namespace :dashboard do

    resources :slides
    resources :settings, only: [:index, :edit, :update]
    resources :categories
    resources :projects
    resources :images

    resources :services
    resources :service_presentation, only: [:update, :destroy]

    resources :about, only: [:index, :edit, :update, :destroy]

    resources :clients

    resources :admins

  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
