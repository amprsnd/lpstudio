class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale

  def set_locale

    if params[:locale] == 'global' || params[:locale] == 'local'
      I18n.locale = :en
    else
      I18n.default_locale
    end

    #I18n.locale = params[:locale] || I18n.default_locale
  end

  layout :devise_layout

  protected

  def devise_layout
    if devise_controller?
      'devise'
    else
      'application'
    end
  end

  def after_sign_in_path_for(resource)
    '/en/dashboard'
  end

end
