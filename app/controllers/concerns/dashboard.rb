module Dashboard

  extend ActiveSupport::Concern

    included do

      layout 'dashboard'
      before_action :authenticate_admin!

    end

end