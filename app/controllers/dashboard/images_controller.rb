class Dashboard::ImagesController < ApplicationController

  include Dashboard

  def index
    @images = Image.all.order id: :desc
  end

  def show
    @image = Image.find params[:id]
  end

  def new
    @image = Image.new
  end

  def create
    @image = Image.new image_params
    params[:image][:image].each do |file|
      @image = Image.new(:image => file,
                         :title => file.original_filename,
                         :description => '')
      @image.save
    end

    if @image.save
      flash[:notice] = 'Images uploaded'
      redirect_to dashboard_images_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new Image'
      render :action => :new
    end
  end

  def edit
    @image = Image.find params[:id]
  end

  def update
    @image = Image.find params[:id]

    if @image.update_attributes image_params
      flash[:notice] = 'Image has been updated'
      redirect_to dashboard_image_path
    else
      flash.now[:warning] = 'There were problems when trying to update this image'
      render :action => :edit
    end
  end

  def destroy
    @image = Image.find params[:id]

    @image.destroy
    flash[:notice] = 'Image has been deleted'
    redirect_to dashboard_images_path
  end

  private
  def image_params
    params.require(:image).permit(:title, :description, :image, :image_file_name)
  end

end
