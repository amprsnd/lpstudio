class Dashboard::SettingsController < ApplicationController

  include Dashboard

  # good idea?..

  def index

    if Setting.exists?
      @settings = Setting.last
    else
      #install default settings
      @settings = Setting.new(description: 'slogan here', contactblock: 'contacts here', )
      @settings.save
    end

  end

  def edit
    @settings = Setting.last
  end

  def update
    @settings = Setting.last

    if params.has_key?(:logo)
    else
      @settings.logo = nil
    end

    if @settings.update_attributes settings_params
      flash[:notice] = 'Settings has been updated'
      redirect_to dashboard_settings_path
    else
      flash.now[:warning] = 'There were problems when trying to update settings'
      render :action => :edit
    end
  end

  private
  def settings_params
    params.require(:setting).permit(:description, :contactblock, :logo, :logo_file_name, :locale)
  end

end
