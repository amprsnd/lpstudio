class Dashboard::ServicesController < ApplicationController

  include Dashboard

  def index
    @services = Service.all
    @service  = Service.new

    @icons = ICONS

    if ServicePresentation.exists?
      @presentation = ServicePresentation.last
    else
      #install default settings
      @presentation = ServicePresentation.new
      @presentation.save
    end

  end

  def show
    @service = Service.find params[:id]
  end

  def new
    @service = Service.new
    @services = Service.all

    @icons = ICONS
  end

  def create
    @service = Service.new services_params

    if @service.save
      flash[:notice] = 'Service created'
      redirect_to dashboard_services_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new service'
      render :action => :new
    end

  end

  def edit
    @service = Service.find params[:id]

    @icons = ICONS
  end

  def update
    @service = Service.find params[:id]

    if @service.update_attributes services_params
      flash[:notice] = 'Service has been updated'
      redirect_to dashboard_services_path
    else
      flash.now[:warning] = 'There were problems when trying to update this service'
      render :action => :edit
    end

  end

  def destroy
    @service = Service.find params[:id]

    @service.destroy
    flash[:notice] = 'Service has been deleted'
    redirect_to dashboard_services_path

  end

  private
  def services_params
    params.require(:service).permit(:title, :description, :icon, :locale, :is_global, :is_local)
  end

end
