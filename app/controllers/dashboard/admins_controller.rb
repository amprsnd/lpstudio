class Dashboard::AdminsController < ApplicationController

  include Dashboard

  def index
    @users = Admin.all
  end

  def show
    @user = Admin.find params[:id]
  end

  def new
    @user = Admin.new
  end

  def create
    @user = Admin.new user_params

    if @user.save
      flash[:notice] = 'User created'
      redirect_to dashboard_admins_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new user'
      render :action => :new
    end
  end

  def edit
    @user = Admin.find params[:id]
  end

  def update
    @user = Admin.find params[:id]

    if @user.update_attributes user_params
      flash[:notice] = 'User has been updated'
      redirect_to dashboard_admins_path
    else
      flash.now[:warning] = 'There were problems when trying to update this user'
      render :action => :edit
    end
  end

  def destroy
    @user = Admin.find params[:id]

    @user.destroy
    flash[:notice] = 'User has been deleted'
    redirect_to dashboard_admins_path
  end

  private
  def user_params
    params.require(:admin).permit(:email, :password, :password_confirmation)
  end

end
