class Dashboard::ClientsController < ApplicationController

  include Dashboard

  def index
    @clients = Client.all
    @client  = Client.new
  end

  def show
    @client = Client.find params[:id]
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new client_params

    if @client.save
      flash[:notice] = 'Client created'
      redirect_to dashboard_clients_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new client'
      render :action => :new
    end
  end

  def edit
    @client = Client.find params[:id]
  end

  def update
    @client = Client.find params[:id]

    if @client.update_attributes client_params
      flash[:notice] = 'Client has been updated'
      redirect_to dashboard_clients_path
    else
      flash.now[:warning] = 'There were problems when trying to update this client'
      render :action => :edit
    end
  end

  def destroy
    @client = Client.find params[:id]

    @client.destroy
    flash[:notice] = 'Client has been deleted'
    redirect_to dashboard_clients_path
  end

  private
  def client_params
    params.require(:client).permit(:name, :description, :logo, :logo_file_name, :project_id, :locale)
  end

end
