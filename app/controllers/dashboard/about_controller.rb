class Dashboard::AboutController < ApplicationController

  include Dashboard

  def index

    if About.exists?
      @about = About.last
    else
      #install default settings
      @about = About.new(description: 'About company here')
      @about.save
    end

  end

  def edit
    @about = About.last
  end

  def update
    @about = About.last

    #if params.has_key?(:presentation)
    #else
    #  @about.presentation = nil
    #end

    if @about.update_attributes about_params
      flash[:notice] = 'About has been updated'
      redirect_to dashboard_about_index_path
    else
      flash.now[:warning] = 'There were problems when trying to update About'
      render :action => :edit
    end

  end

  #only for deleting attachment
  def destroy
    @about = About.find params[:id]

    @about.presentation_en = nil
    @about.presentation_ru = nil
    @about.save
    flash[:notice] = 'Presentation file has been deleted'
    redirect_to dashboard_about_index_path

  end

  private
  def about_params
    params.require(:about).permit(:description,
                                  :presentation_en, :presentation_file_name_en,
                                  :presentation_ru, :presentation_file_name_ru,
                                  :locale
    )
  end

end
