class Dashboard::ProjectsController < ApplicationController

  include Dashboard

  def index
    @projects = Project.all.order(id: :desc)
  end

  def show
    @project = Project.find params[:id]
  end

  def new
    @project = Project.new

    relations
  end

  def create
    @project = Project.new project_params

    relations

    if @project.save
      flash[:notice] = 'Project created'
      redirect_to dashboard_projects_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new project'
      render :action => :new
    end

  end

  def edit
    @project = Project.find params[:id]

    relations

  end

  def update
    @project = Project.find params[:id]

    relations

    if @project.update_attributes project_params
      flash[:notice] = 'Project has been updated'
      redirect_to dashboard_projects_path
    else
      flash.now[:warning] = 'There were problems when trying to update this project'
      render :action => :edit
    end
  end

  def destroy
    @project = Project.find params[:id]

    @project.destroy
    flash[:notice] = 'Project has been deleted'
    redirect_to dashboard_projects_path
  end

  #for relations
  def relations
    #@categories = Category.all
    @images = Image.all.order id: :desc
  end

  private
  def project_params
    params.require(:project).permit(:title,
                                    :description,
                                    :area,
                                    :address,
                                    :client_name,
                                    :team,
                                    :year,
                                    :category_id,
                                    :locale,
                                    :is_public,
                                    :is_global,
                                    :is_local,
                                    {image_ids:[]}
    )
  end

end
