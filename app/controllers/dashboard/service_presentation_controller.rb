class Dashboard::ServicePresentationController < ApplicationController

  include Dashboard

  def edit
    @presentation = ServicePresentation.last
  end

  def update
    @presentation = ServicePresentation.last
    if @presentation.update_attributes presentation_params
      flash[:notice] = 'Presentation Updated'
    else
      flash.now[:warning] = 'There were problems when trying to update Presentation'
    end

    redirect_to :back

  end

  def destroy
    @presentation = ServicePresentation.last
    @presentation.presentation_en = nil
    @presentation.presentation_ru = nil
    @presentation.save

    flash[:notice] = 'Presentation file has been deleted'
    redirect_to :back

  end

  private
  def presentation_params
    params.require(:service_presentation).permit(:presentation_en, :presentation_file_name_en,
                                                 :presentation_ru, :presentation_file_name_ru,
                                                 :locale
    )
  end

end
