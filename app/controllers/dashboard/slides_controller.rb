class Dashboard::SlidesController < ApplicationController

  include Dashboard

  def index
    @slides = Slide.all
  end

  def show
    @slide =  Slide.find params[:id]
  end

  def new
    @slide =  Slide.new
  end

  def create
    @slide =  Slide.new slide_params

    if @slide.save
      flash[:notice] = 'Slide created'
      redirect_to dashboard_slides_path
    else
      flash.now[:warning] = 'There were problems when trying to create a new Slide'
      render :action => :new
    end
  end

  def edit
    @slide =  Slide.find params[:id]
  end

  def update
    @slide =  Slide.find params[:id]

    if @slide.update_attributes slide_params
      flash[:notice] = 'Slide has been updated'
      redirect_to dashboard_slides_path
    else
      flash.now[:warning] = 'There were problems when trying to update this slide'
      render :action => :edit
    end
  end

  def destroy
    @slide =  Slide.find params[:id]

    @slide.destroy
    flash[:notice] = 'Slide has been deleted'
    redirect_to dashboard_slides_path
  end

  private
  def slide_params
    params.require(:slide).permit(:title, :description, :slide, :slide_file_name)
  end

end
