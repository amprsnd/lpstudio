class OutputController < ApplicationController
  def index

    #first slide
    @slides = Slide.all

    #global information block
    @settings = Setting.last

    #services slide

    if params[:locale] == 'local'
      @services = Service.where(is_local: true)
      @projects = Project.where(is_local: true)  #.order id: :desc
    else
      @services = Service.where(is_global: true)
      @projects = Project.where(is_global: true) #.order id: :desc
    end

    #@services = Service.all
    @service_presentation = ServicePresentation.last

    #portfolio slide
    @categories = Category.all
    #@projects = Project.all.order id: :desc

    #about slide
    @about = About.last

    #Clients slide
    @clients = Client.all

  end

  def show

    @project = Project.find params[:id]

  end

end
