class Slide < ActiveRecord::Base

  #validations
  validates :slide, presence: { message: 'Sorry, can`t be blank' }

  #relations

  #images
  has_attached_file :slide,
                    styles: {
                        large: '1920x1080>',
                        medium:  '1366x768>',
                        small:  '1024x768>',
                        thumb: '640x480'
                    },
                    default_url: '/images/notfound.png'
  validates_attachment_content_type :slide, content_type: /\Aimage\/.*\Z/

end
