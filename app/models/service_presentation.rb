class ServicePresentation < ActiveRecord::Base

  #validations

  #presentation file
  has_attached_file :presentation_en
  validates_attachment :presentation_en, content_type: { content_type: 'application/pdf' }

  #presentation file
  has_attached_file :presentation_ru
  validates_attachment :presentation_ru, content_type: { content_type: 'application/pdf' }

end
