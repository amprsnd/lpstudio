class Project < ActiveRecord::Base

  self.default_scope { order(id: :desc) }

  #translations
  translates :title, :client_name, :address, :team, :description

  #validations
  validates :title, presence: { message: 'Sorry, can`t be blank' }
  validates :title, length: {
                      minimum: 2,
                      maximum: 128,
                      too_short: 'Title is too short...',
                      too_long:  'Title is too long...'
                  }
  validates :category_id, presence: { message: 'hmm, what about category?..' }
  validates :image_ids, presence: { message: "don't forget images" }
  validates :year, length: { is: 4 , message: 'wrong...'}, numericality: true, presence: true

  #relations
  belongs_to :category
  has_one :client
  has_many :images

end
