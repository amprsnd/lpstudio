class Client < ActiveRecord::Base

  #translations
  translates :name, :description

  #validations
  validates :logo, presence: { message: 'Sorry, can`t be blank' }

  #relations
  belongs_to :project

  #image
  has_attached_file :logo,
                    styles: {
                        normal: 'x400>',
                        small:  'x200>'

                    },
                    default_url: '/images/notfound.png'
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/

end
