class Setting < ActiveRecord::Base

  #translations
  translates :contactblock, :description

  #validations
  validates :contactblock, presence: true

  #relations

  #images
  has_attached_file :logo,
                    styles: {
                        original: '400x200>',
                        normal: '250x200>'
                    },
                    default_url: '/images/logo.png'
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/

end
