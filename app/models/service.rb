class Service < ActiveRecord::Base

  #translations
  translates :title, :description

  #validations
  validates :title, :description, :icon, presence: { message: 'Sorry, can`t be blank' }

end
