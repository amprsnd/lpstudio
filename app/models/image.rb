class Image < ActiveRecord::Base

  #validations
  validates :image, presence: { message: 'Sorry, can`t be blank' }

  #relations
  belongs_to :project

  #images
  has_attached_file :image,
                    styles: {
                        large:      ['x1080>', :jpg],
                        medium:     ['x768>', :jpg],
                        small:      ['x768>', :jpg],
                        thumb:      ['400x300#', :jpg],
                        pizdulina:  ['x100', :jpg]
                    },
                    convert_options: { all: '-quality 70' },
                    default_url: '/images/notfound.png'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

end
