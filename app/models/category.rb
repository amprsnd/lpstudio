class Category < ActiveRecord::Base

  #translations
  translates :title, :description

  #validations
  validates :title, presence: { message: 'Sorry, can`t be blank' }
  validates :title, length: {
                      minimum: 2,
                      maximum: 128,
                      too_short: 'Title is too short...',
                      too_long:  'Title is too long...'
                  }
  validates :title, uniqueness: { message: 'You already have this title' }

  #relations
  has_many :projects

end
