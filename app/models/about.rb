class About < ActiveRecord::Base

  #translations
  translates :description

  #validations
  validates :description, presence: { message: 'Sorry, can`t be blank' }

  #presentation files
  has_attached_file :presentation_en
  validates_attachment :presentation_en, content_type: { content_type: 'application/pdf' }

  #presentation files
  has_attached_file :presentation_ru
  validates_attachment :presentation_ru, content_type: { content_type: 'application/pdf' }

end
