module OutputHelper

  #lightbox project info
  def project_info (field, title)
    field.blank? ? '' : ("<b>#{title}:</b> #{field}").html_safe
  end


end
