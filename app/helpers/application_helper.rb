module ApplicationHelper


  def get_logo

    if Setting.last == nil
      output = image_tag '/images/logo.png', alt: 'logo', class: 'responsive-img'
    else
      logo = Setting.last.logo.url(:normal)
      output = image_tag logo, alt: 'logo', class: 'responsive-img'
    end

    output
  end

end
