$(function() {

    var fullPage = $('#fullpage');

    //init fullpage plugin
    fullPage.fullpage({
        lockAnchors: false,
        anchors:['welcome', 'services', 'portfolio', 'about', 'clients'],
        autoScrolling: false,
        //normalScrollElements: '#welcome-screen, #services-screen, #portfolio-screen, #about-screen, #clients-screen',
        //controlArrows: true,
        //verticalCentered: true,
        scrollBar: true,
        //slidesNavigation: true,
        //scrollOverflow: true,
        fitToSection: false
        //resize : false,
    });

    //Show/hide blocks
    $(document).scroll(function() {
        var scrollPos = $(document).scrollTop();
        var contacts = $('.contacts'), contactsTab = $('.contacts .tab');
        var navItem = $('nav a'), navItemTab = $('nav a .tab');

        if (scrollPos > 10) {
            //hide blocks
            contacts.css('margin-left', '-255px');
            contactsTab.css( {opacity:'1', visibility: 'visible'});

            navItem.each( function(){
                var width = $(this).width()-20;
                $(this).css('margin-right', '-' + width + 'px');
                $(this).addClass('folded');
            });
            navItemTab.css({opacity: '1', width: 'initial'});

        } else {
            //show blocks
            contacts.css('margin-left', '0');
            contactsTab.css( 'opacity','0');
            setTimeout(function(){
                contactsTab.css( 'visibility','hidden');
            }, 500);

            navItem.each( function(){
                $(this).css('margin-right', '0px');
                $(this).removeClass('folded');
            });

            navItemTab.css('opacity', '0');

        }

        if($(window).scrollTop() + $(window).height() == $(document).height()) {

            $('.contacts, nav').css('bottom', '110px');

        } else {

            $('.contacts, nav').css('bottom', '10px');

        }

    });

    //fix for navigation animation
    var navLink = $('nav a');
    navLink.css('right', '0px');

    //Show/Hide Contacts
    $('.contacts .tab').on('click', function() {
        var parent = $(this).parent();

        if (parent.css('margin-left') != '0px') {
            parent.css('margin-left', '0px')
        } else {
            parent.css('margin-left', '-255px')
        }

    });

    //fix scrolling navigation
    navLink.click(function(e) {
        var navHash = $(this).attr('href');
        window.location.hash = navHash;

        $('body').scrollTo($(navHash + '-screen'), 750);

        e.preventDefault();
    });

    //Fix fullpage bug after lang switch
    /*
    $('.lang-switcher a').on('click', function(e) {

        window.location.href = $(this).attr('href');
        e.preventDefault();

    });
    */
    //portfolio grid
    /*
    $(".portfolio-wall").gridalicious({
        selector: '.item',
        gutter: 1,
        width: 250,
        animate: true,
        animationOptions: {
            queue: true,
            speed: 200,
            duration: 300,
            effect: 'fadeInOnAppear'
        }
    });
    */
    /*
    $('.portfolio-wall').justifiedGallery({
        //rowHeight: 75,
        maxRowHeight: '250%',
        lastRow : 'nojustify',
        margins : 3,
        fixedHeight: false
    });
    */

    //Lightbox not needed

    //Image adaptation in slider
    /*
    function sliderImage() {
        var imageContainer = $('.item');

        imageContainer.each(function() {

            //check image is loaded?
            $(this).children().one('load', function(){

                var parentHeight = $(this).parent().height(),
                    selfWidth    = $(this).width(),
                    selfHeight   = $(this).height(),
                    self         = $(this);
                console.log(
                    'Parent class: '    + $(this).parent().attr('class') +
                    ', Parent height: ' + parentHeight +
                    ', SelfWidth: '     + selfWidth +
                    ', SelfHeight: '    + selfHeight +
                    '. My name is: '    + self.attr('class') + '.'
                );

            });
        });
    }


    sliderImage();
    */
    //carousel
    function initSlider() {
        $(".image-slider-wrapper").lightSlider({
            adaptiveHeight: true,
            item: 1,
            slideMargin: 0,
            loop: true
        });
    }

    initSlider();


    //init slider on load lightbox
    var lightBoxLinks = $(' a.portfolio-project, a.client-project');

    lightBoxLinks.featherlight({
        targetAttr: 'href',
        onResize: function(){

            console.log('Slider: I`am here!'); // this contains all related elements
            initSlider();

            }
    });


});
