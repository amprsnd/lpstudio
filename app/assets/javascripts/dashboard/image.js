/**
 * Created by ampersand on 11/15/15.
 */

$(function() {

    var file = $('input[name="image[image][]"]');

    $('.control-buttons input[type="submit"]').on('click', function() {
        if (file.val().length == 0) {
            alert('No files for upload...')
        } else {

            $('.control-buttons').css('display', 'none');
            $('.upload-progress').css('display', 'block');
        }
    });



});
