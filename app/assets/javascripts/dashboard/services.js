$(function() {

    $('.modal-trigger').leanModal();


    $('.set-icon').on('click', function(e){

        var icon = $(this).attr('data-icon'), preview = $('.select-icon-block .icon-preview');

        $('#icon-value').val(icon);

        preview.removeClass('material-icons');
        preview.text();

        preview.addClass('material-icons');
        preview.text(icon);

        e.preventDefault();

    });

});
