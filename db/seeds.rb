# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


@services = Service.all
@projects = Project.all

@services.each do |service|

  service.is_global = true
  service.is_local = true
  service.save

end

@projects.each do |project|

  project.is_global = true
  project.is_local = true
  project.save

end

=begin
@projects_public = Project.all

@projects_public.each do |project|

  project.is_public = true
  project.save

end
=end
