class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :title
      t.string :description
      t.string :icon

      t.timestamps null: false
    end
  end
end
