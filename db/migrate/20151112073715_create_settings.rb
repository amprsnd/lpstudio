class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :description
      t.string :contactblock

      t.timestamps null: false
    end
  end
end
