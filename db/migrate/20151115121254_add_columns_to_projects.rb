class AddColumnsToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :area, :string
    add_column :projects, :client, :string
    add_column :projects, :year, :string
    add_column :projects, :address, :string
    add_column :projects, :team, :string
  end
end
