class RenameClientInProject < ActiveRecord::Migration
  def change
    rename_column :projects, :client, :client_name
  end
end
