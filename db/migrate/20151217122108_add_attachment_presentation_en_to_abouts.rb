class AddAttachmentPresentationEnToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :presentation_en
    end
  end

  def self.down
    remove_attachment :abouts, :presentation_en
  end
end
