class AddAttachmentPresentationToServicePresentations < ActiveRecord::Migration
  def self.up
    change_table :service_presentations do |t|
      t.attachment :presentation
    end
  end

  def self.down
    remove_attachment :service_presentations, :presentation
  end
end
