class AddOptionsToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :is_global, :boolean
    add_column :projects, :is_local, :boolean
  end
end
