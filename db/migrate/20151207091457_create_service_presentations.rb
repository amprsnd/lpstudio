class CreateServicePresentations < ActiveRecord::Migration
  def change
    create_table :service_presentations do |t|

      t.timestamps null: false
    end
  end
end
