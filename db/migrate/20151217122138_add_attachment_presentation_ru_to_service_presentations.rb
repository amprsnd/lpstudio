class AddAttachmentPresentationRuToServicePresentations < ActiveRecord::Migration
  def self.up
    change_table :service_presentations do |t|
      t.attachment :presentation_ru
    end
  end

  def self.down
    remove_attachment :service_presentations, :presentation_ru
  end
end
