class AddAttachmentPresentationToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :presentation
    end
  end

  def self.down
    remove_attachment :abouts, :presentation
  end
end
