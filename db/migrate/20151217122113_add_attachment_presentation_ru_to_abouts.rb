class AddAttachmentPresentationRuToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :presentation_ru
    end
  end

  def self.down
    remove_attachment :abouts, :presentation_ru
  end
end
