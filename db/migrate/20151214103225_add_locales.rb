class AddLocales < ActiveRecord::Migration
  def up
    Category.create_translation_table! :title => :string, :description => :string
    Project.create_translation_table! :title => :string,
                                      :client_name => :string,
                                      :address => :string,
                                      :team => :string,
                                      :description => :string
    About.create_translation_table! :description => :string
    Service.create_translation_table! :title => :string, :description => :string
    Client.create_translation_table! :name => :string, :description => :string
    Setting.create_translation_table! :contactblock => :string, :description => :string
  end
  def down
    Category.drop_translation_table!
    Project.drop_translation_table!
    About.drop_translation_table!
    Service.drop_translation_table!
    Client.drop_translation_table!
    Setting.drop_translation_table!
  end
end
