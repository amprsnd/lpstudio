class AddAttachmentPresentationEnToServicePresentations < ActiveRecord::Migration
  def self.up
    change_table :service_presentations do |t|
      t.attachment :presentation_en
    end
  end

  def self.down
    remove_attachment :service_presentations, :presentation_en
  end
end
