class AddColumnsToServices < ActiveRecord::Migration
  def change
    add_column :services, :is_global, :boolean
    add_column :services, :is_local, :boolean
  end
end
